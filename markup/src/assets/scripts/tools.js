$(document).ready(function(){
  $('.trigger-step-2').on("click", function(){
    $('.step-container').removeClass('active');
    $('#registration-step-2').addClass('active');
  });
  $('.trigger-step-3').on("click", function(){
    $('.step-container').removeClass('active');
    $('#registration-step-3').addClass('active');
  });

  $('.popup__closer').on("click", function(){
    $('.popups__wr, .popup').removeClass('active');
  });

  $('.mobile-nav-trigger').on("click", function () {
    $(this).toggleClass('active');
    $('.header__nav').toggleClass('active');
  });
});